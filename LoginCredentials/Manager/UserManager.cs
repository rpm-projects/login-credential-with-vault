﻿
using LoginCredentials.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Credentials;

namespace LoginCredentials.Manager
{
    class UserManager
    {

        public static void addNewUser(User user)
        {
           saveCredential(user);
        }

        public static PasswordCredential findCredential(User user)
        {
            var vault = new Windows.Security.Credentials.PasswordVault();
            var credentialList = vault.FindAllByResource(user.feature);

            foreach (PasswordCredential cre in credentialList)
            {
                if (cre.UserName == user.username)
                {
                    return cre;
                }
            }

            return null;
        }

        public static bool authenticate(User user)
        {
            var vault = new Windows.Security.Credentials.PasswordVault();
            var credentialList = vault.FindAllByResource(user.feature);

            foreach (PasswordCredential cre in credentialList)
            {
                if (cre.UserName == user.username && cre.Password == user.password)
                {
                    return true;
                }
            }

            return false;
        }

        public static void removeCredential(PasswordCredential cre)
        {
            var vault = new Windows.Security.Credentials.PasswordVault();
            vault.Remove(cre);
        }


        public static void saveCredential(User user)
        {
            var vault = new Windows.Security.Credentials.PasswordVault();

            //If there is already a user registered with same username then remove it and create a new one
            PasswordCredential cre = findCredential(user);
            if (cre != null)
            {
                removeCredential(cre);
            }

            vault.Add(new Windows.Security.Credentials.PasswordCredential(
            user.feature, user.username, user.password));
            var credentialList = vault.FindAllByResource(user.feature);

            foreach (PasswordCredential cre2 in credentialList)
            {
                var test = cre2;
            }

        }

        public static List<User> getAllUsers()
        {
            List<User> usersList = new List<User>();
            var vault = new Windows.Security.Credentials.PasswordVault();
            IReadOnlyList<PasswordCredential> creList = vault.FindAllByResource("WinApp");

            foreach (PasswordCredential cre in creList)
            {
                usersList.Add(new User
                {
                    username = cre.UserName,
                    password = cre.Password,
                    feature = "WinApp"
                });
            }

            return usersList;
        }
    }
}
