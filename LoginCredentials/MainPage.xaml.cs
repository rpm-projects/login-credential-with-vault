﻿
using LoginCredentials.Manager;
using LoginCredentials.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Security.Credentials;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace LoginCredentials
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        //ObservableCollection<User> usersList = new ObservableCollection<User>();

        public MainPage()
        {
            this.InitializeComponent();
            initialListLoad();
        }

        public void initialListLoad()
        {
            UsersListView.ItemsSource = UserManager.getAllUsers(); ;
        }

        public void addNewUser(object sender, RoutedEventArgs e)
        {

            User user = new User()
            {
                username = textBox_username.Text,
                password = textBox_password.Text,
                feature = "WinApp"
            };

            UserManager.addNewUser(user);
            UsersListView.ItemsSource = UserManager.getAllUsers();
        }

        public void deleteUser(object sender, RoutedEventArgs e)
        {

            if (UsersListView.SelectedItem != null)
            {
                User user = UsersListView.SelectedItem as User;
                PasswordCredential cre = UserManager.findCredential(user);
                UserManager.removeCredential(cre);
                UsersListView.ItemsSource = UserManager.getAllUsers();
            }
        }

    }
}
