﻿using LoginCredentials.Manager;
using LoginCredentials.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace LoginCredentials
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginView : Page
    {
        public LoginView()
        {
            this.InitializeComponent();
        }


        //Try to log in
        private void button_Click(object sender, RoutedEventArgs e)
        {
            User user = new User { username = textBox_username.Text,
                                   password = passwordBox.Password,
                                   feature = "WinApp"};
            UserManager.authenticate(user);

            //Redirect to main page
            this.Frame.Navigate(typeof(MainPage));
        }
    }
}
